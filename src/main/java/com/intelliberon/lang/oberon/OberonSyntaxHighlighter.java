package com.intelliberon.lang.oberon;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class OberonSyntaxHighlighter extends SyntaxHighlighterBase {

	public static final TextAttributesKey NUMBER = createTextAttributesKey("Oberon number", DefaultLanguageHighlighterColors.NUMBER);
	public static final TextAttributesKey KEYWORD = createTextAttributesKey("Oberon keyword", DefaultLanguageHighlighterColors.KEYWORD);
	public static final TextAttributesKey STD_FUNC = createTextAttributesKey("Oberon standard function", DefaultLanguageHighlighterColors.KEYWORD);
	public static final TextAttributesKey COMMENT = createTextAttributesKey("Oberon comment", DefaultLanguageHighlighterColors.BLOCK_COMMENT);
	public static final TextAttributesKey STRING = createTextAttributesKey("Oberon string", DefaultLanguageHighlighterColors.STRING);

	// TODO for the following keys, create a meaningful TextAttributeKey instead of using CLASS_NAME
	public static final TextAttributesKey PRIMITIVE_TYPE = createTextAttributesKey("OBERON_PRIMITIVE_TYPE", DefaultLanguageHighlighterColors.KEYWORD);
	public static final TextAttributesKey SYSTEM_FUNC = createTextAttributesKey("OBERON_SYSTEM_FUNCTION", DefaultLanguageHighlighterColors.KEYWORD);

	private static final TextAttributesKey[] NUMBER_KEYS = new TextAttributesKey[]{NUMBER};
	private static final TextAttributesKey[] KEYWORD_KEYS = new TextAttributesKey[]{KEYWORD};
	private static final TextAttributesKey[] STD_FUNC_KEYS = new TextAttributesKey[]{STD_FUNC};
	private static final TextAttributesKey[] PRIMITIVE_TYPES_KEYS = new TextAttributesKey[]{PRIMITIVE_TYPE};
	private static final TextAttributesKey[] SYSTEM_FUNC_KEYS = new TextAttributesKey[]{SYSTEM_FUNC};

	private static final TextAttributesKey[] COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
	private static final TextAttributesKey[] STIRNG_KEYS = new TextAttributesKey[]{STRING};

	private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

	@NotNull
	@Override
	public Lexer getHighlightingLexer() {
		return new OberonLexerAdapter();
	}


	/**
	 * Would be better to implement this with {@code HashMap<IElementType, TextAttributeKey>()};
	 */
	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        // keywords
		if (OberonTokenTypes.KEYWORDS.contains(tokenType)){
		    return KEYWORD_KEYS;
        // standard functions
        } else if (OberonTokenTypes.STD_FUNC.contains(tokenType))
        {
            return STD_FUNC_KEYS;
        } else if (OberonTokenTypes.PRIMITIVE_TYPES.contains(tokenType))
        {
            return PRIMITIVE_TYPES_KEYS;
        } else if (tokenType.equals(OberonTokenTypes.SYSTEM_FUNC)) {
		    return SYSTEM_FUNC_KEYS;
        } else if (tokenType.equals(OberonTokenTypes.COMMENT)) {
            return COMMENT_KEYS;
        } else if (tokenType.equals(OberonTokenTypes.STRING)) {
            return STIRNG_KEYS;
        } else if(tokenType.equals(OberonTokenTypes.NUMBER)) {
			return NUMBER_KEYS;
		} else {
			return EMPTY_KEYS;
		}
	}
}
