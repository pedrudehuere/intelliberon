package com.intelliberon.lang.oberon;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class OberonIcons
{
	public static final Icon FILE = IconLoader.getIcon("/icons/pirate.png");
	public static final Icon CONF_FILE = IconLoader.getIcon("/icons/pirate.png");
	public static final Icon EDS_FILE = IconLoader.getIcon("/icons/pirate.png");
}
