package com.intelliberon.lang.oberon;


import com.intelliberon.lang.oberon.OberonSyntaxHighlighter;
import com.intelliberon.lang.oberon.OberonIcons;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.*;
import org.jetbrains.annotations.*;

import javax.swing.*;
import java.util.Map;

public class OberonColorSettingsPage implements ColorSettingsPage
{
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Number", OberonSyntaxHighlighter.NUMBER),
            new AttributesDescriptor("Keyword", OberonSyntaxHighlighter.KEYWORD),
            new AttributesDescriptor("Standard function", OberonSyntaxHighlighter.STD_FUNC),
            new AttributesDescriptor("Comment", OberonSyntaxHighlighter.COMMENT),
            new AttributesDescriptor("String", OberonSyntaxHighlighter.STRING),
            new AttributesDescriptor("Primitive type", OberonSyntaxHighlighter.PRIMITIVE_TYPE),
            new AttributesDescriptor("SYSTEM function", OberonSyntaxHighlighter.SYSTEM_FUNC)
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return OberonIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new OberonSyntaxHighlighter();
    }


    @NotNull
    @Override
    public String getDemoText() {
        return "(* Comment *)\n" +
                "\"Hello!\";\n" +
                "MyInt*: LONGINT;\n" +
                "MyInt := SYSTEM.ADR(43000H);\n" +
                "INC(MyInt);\n" +
                "MyFloat*: LONGREAL;\n" +
                "MyFLoat := 43.3E+3;\n" +
                "MyString: POINTER TO ARRAY OF CHAR;\n";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Oberon";
    }
}
