package com.intelliberon.lang.oberon;

import com.intellij.psi.tree.IElementType;
import com.intelliberon.lang.oberon.OberonLanguage;
import org.jetbrains.annotations.*;

public class OberonElementType extends IElementType {
  public OberonElementType(@NotNull @NonNls String debugName) {
    super(debugName, OberonLanguage.INSTANCE);
  }
}