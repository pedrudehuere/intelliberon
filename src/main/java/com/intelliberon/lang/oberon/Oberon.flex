package com.intelliberon.lang.oberon;

import com.intelliberon.lang.BaseLexer;

import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;

/*
 * Lexer for syntax highlighting
 */

%%

%class OberonLexer
%extends BaseLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}


%{
    // The start of a string should be " or '
    String stringStart = null;
%}


LINE_TERMINATOR = \n|\r\n|\r
WHITE_SPACE = { LINE_TERMINATOR } | [ \t\f]
INPUT_CHARACTER = [^\n\r]

DIGIT = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
LETTER = [:jletter:]
HEX_DIGIT = {DIGIT} | "A" | "B" | "C" | "D" | "E" | "F"
NUMBER = {INTEGER} | {REAL}
INTEGER = {DIGIT}+ | {DIGIT} {HEX_DIGIT}* "H"
REAL = {DIGIT}+ "." {DIGIT}* {SCALE_FACTOR}*
SCALE_FACTOR = ("E" | "D") ["+" | "-"] {DIGIT}+

IDENTIFIER=[:jletter:] [:jletterdigit:]*

COMMENT_START = "(*"
COMMENT_END = "*)"

DOUBLE_QUOTE = \"
SINGLE_QUOTE = "'"
SYSTEM_FUNC = SYSTEM\.[A-Z]+

%state IN_COMMENT
%state IN_STRING

%%


<YYINITIAL> {
    // Keywords
    "IF"                { return OberonTokenTypes.IF; }
    "THEN"              { return OberonTokenTypes.THEN; }
    "ELSE"              { return OberonTokenTypes.ELSE; }
    "ELSIF"             { return OberonTokenTypes.ELSIF;}
    "BEGIN"             { return OberonTokenTypes.BEGIN; }
    "END"               { return OberonTokenTypes.END; }
    "MODULE"            { return OberonTokenTypes.MODULE; }
    "RECORD"            { return OberonTokenTypes.RECORD; }
    "VAR"               { return OberonTokenTypes.VAR; }
    "CONST"             { return OberonTokenTypes.CONST; }
    "RETURN"            { return OberonTokenTypes.RETURN; }
    "FOR"               { return OberonTokenTypes.FOR; }
    "ABS"               { return OberonTokenTypes.ABS; }
    "ARRAY"             { return OberonTokenTypes.ARRAY; }
    "ASR"               { return OberonTokenTypes.ASR; }
    "ASSERT"            { return OberonTokenTypes.ASSERT; }
    "BEGIN"             { return OberonTokenTypes.BEGIN; }
    "BOOLEAN"           { return OberonTokenTypes.BOOLEAN; }
    "BY"                { return OberonTokenTypes.BY; }
    "BYTE"              { return OberonTokenTypes.BYTE; }
    "CASE"              { return OberonTokenTypes.CASE; }
    "CHAR"              { return OberonTokenTypes.CHAR; }
    "CHR"               { return OberonTokenTypes.CHR; }
    "CONST"             { return OberonTokenTypes.CONST; }
    "DEC"               { return OberonTokenTypes.DEC; }
    "DIV"               { return OberonTokenTypes.INT_DIV; }
    "DO"                { return OberonTokenTypes.DO; }
    "ELSE"              { return OberonTokenTypes.ELSE; }
    "ELSIF"             { return OberonTokenTypes.ELSIF; }
    "END"               { return OberonTokenTypes.END; }
    "EXCL"              { return OberonTokenTypes.EXCL; }
    "FALSE"             { return OberonTokenTypes.FALSE; }
    "FLOOR"             { return OberonTokenTypes.FLOOR; }
    "FLT"               { return OberonTokenTypes.FLT; }
    "FOR"               { return OberonTokenTypes.FOR; }
    "IF"                { return OberonTokenTypes.IF; }
    "IMPORT"            { return OberonTokenTypes.IMPORT; }
    "IN"                { return OberonTokenTypes.IN; }
    "INC"               { return OberonTokenTypes.INC; }
    "INCL"              { return OberonTokenTypes.INCL; }
    "INTEGER"           { return OberonTokenTypes.INTEGER; }
    "IS"                { return OberonTokenTypes.IS; }
    "LEN"               { return OberonTokenTypes.LEN; }
    "LONGINT"           { return OberonTokenTypes.LONGINT; }
    "LONGREAL"           { return OberonTokenTypes.LONGREAL; }
    "LSL"               { return OberonTokenTypes.LSL; }
    "MOD"               { return OberonTokenTypes.MOD; }
    "MODULE"            { return OberonTokenTypes.MODULE; }
    "NEW"               { return OberonTokenTypes.NEW; }
    "NIL"               { return OberonTokenTypes.NIL; }
    "ODD"               { return OberonTokenTypes.ODD; }
    "OF"                { return OberonTokenTypes.OF; }
    "OR"                { return OberonTokenTypes.OR; }
    "ORD"               { return OberonTokenTypes.ORD; }
    "PACK"              { return OberonTokenTypes.PACK; }
    "POINTER"           { return OberonTokenTypes.POINTER; }
    "PROCEDURE"         { return OberonTokenTypes.PROCEDURE; }
    "REAL"              { return OberonTokenTypes.REAL; }
    "RECORD"            { return OberonTokenTypes.RECORD; }
    "REPEAT"            { return OberonTokenTypes.REPEAT; }
    "RETURN"            { return OberonTokenTypes.RETURN; }
    "ROR"               { return OberonTokenTypes.ROR; }
    "SET"               { return OberonTokenTypes.SET; }
    "THEN"              { return OberonTokenTypes.THEN; }
    "TO"                { return OberonTokenTypes.TO; }
    "TRUE"              { return OberonTokenTypes.TRUE; }
    "TYPE"              { return OberonTokenTypes.TYPE; }
    "UNPK"              { return OberonTokenTypes.UNPK; }
    "UNTIL"             { return OberonTokenTypes.UNTIL; }
    "VAR"               { return OberonTokenTypes.VAR; }
    "WHILE"             { return OberonTokenTypes.WHILE; }


    // Relations
    "="             { return OberonTokenTypes.EQ; }
    ">"             { return OberonTokenTypes.GT; }
    "<"             { return OberonTokenTypes.LT; }
    ">="            { return OberonTokenTypes.GE; }
    "<="            { return OberonTokenTypes.LE; }
    "#"             { return OberonTokenTypes.NE; }
    "IN"            { return OberonTokenTypes.IN; }
    "IS"            { return OberonTokenTypes.IS; }

    // Punctuation
    "."             { return OberonTokenTypes.PERIOD; }
    ";"             { return OberonTokenTypes.SEMICOLON; }
    "("             { return OberonTokenTypes.LPAREN; }
    ")"             { return OberonTokenTypes.RPAREN; }
    "["             { return OberonTokenTypes.LSBRACK; }
    "]"             { return OberonTokenTypes.RSBRACK; }
    "{"             { return OberonTokenTypes.LCBRACK; }
    "}"             { return OberonTokenTypes.RCBRACK; }

    // Operators
    ":="            { return OberonTokenTypes.ASSIGNEMENT; }
    "~"             { return OberonTokenTypes.NEG; }
    "+"             { return OberonTokenTypes.PLUS; }
    "-"             { return OberonTokenTypes.MINUS; }
    "&"             { return OberonTokenTypes.AND; }
    "*"             { return OberonTokenTypes.MUL; }
    "DIV"           { return OberonTokenTypes.INT_DIV; }
    "/"             { return OberonTokenTypes.INT_DIV; }
    "MOD"           { return OberonTokenTypes.MOD; }

    // Constants
    "TRUE"          { return OberonTokenTypes.TRUE; }
    "FALSE"         { return OberonTokenTypes.FALSE; }
    "NIL"           { return OberonTokenTypes.NIL; }

    {COMMENT_START} { yypushState(IN_COMMENT); return OberonTokenTypes.COMMENT; }

    {SINGLE_QUOTE}  { stringStart = "'"; yypushState(IN_STRING); return OberonTokenTypes.STRING; }
    {DOUBLE_QUOTE}  { stringStart = "\"";  yypushState(IN_STRING); return OberonTokenTypes.STRING; }

    {WHITE_SPACE}+  { return TokenType.WHITE_SPACE; }

    {NUMBER}        { return OberonTokenTypes.NUMBER; }

    {SYSTEM_FUNC} { return OberonTokenTypes.SYSTEM_FUNC; }

    {IDENTIFIER}    { return OberonTokenTypes.IDENTIFIER; }
}

<IN_COMMENT> {
    // comments can be nested
    {COMMENT_START} { yypushState(IN_COMMENT); return OberonTokenTypes.COMMENT; }
    {COMMENT_END}   { yypopState(); return OberonTokenTypes.COMMENT; }
    .               { return OberonTokenTypes.COMMENT; }
}

<IN_STRING> {
    {DOUBLE_QUOTE}  {
        if (stringStart == "\"")
        {
            yypopState();
        }
        return OberonTokenTypes.STRING;
    }
    {SINGLE_QUOTE}  {
        if (stringStart == "'")
        {
            yypopState();
        }
        return OberonTokenTypes.STRING;
    }
    .               { return OberonTokenTypes.STRING; }
}

[^]                 { return TokenType.BAD_CHARACTER; }
