package com.intelliberon.lang.oberon;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class OberonLexerAdapter extends FlexAdapter {
  public OberonLexerAdapter() {
    super(new OberonLexer((Reader) null));
  }
}
