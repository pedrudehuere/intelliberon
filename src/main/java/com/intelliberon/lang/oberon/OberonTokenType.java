package com.intelliberon.lang.oberon;

import com.intellij.psi.tree.IElementType;
import com.intelliberon.lang.oberon.OberonLanguage;
import org.jetbrains.annotations.*;

public class OberonTokenType extends IElementType {
  public OberonTokenType(@NotNull @NonNls String debugName) {
    super(debugName, OberonLanguage.INSTANCE);
  }

  @Override
  public String toString() {
    return "OberonTokenType." + super.toString();
  }
}