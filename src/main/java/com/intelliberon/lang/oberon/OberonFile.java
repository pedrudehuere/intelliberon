package com.intelliberon.lang.oberon;

import com.intelliberon.lang.oberon.OberonLanguage;
import com.intelliberon.lang.oberon.OberonFileType;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class OberonFile extends PsiFileBase {
  public OberonFile(@NotNull FileViewProvider viewProvider) {
    super(viewProvider, OberonLanguage.INSTANCE);
  }

  @NotNull
  @Override
  public FileType getFileType() {
    return OberonFileType.INSTANCE;
  }

  @Override
  public String toString() {
    return "Oberon2 File";
  }

  @Override
  public Icon getIcon(int flags) {
    return super.getIcon(flags);
  }
}