package com.intelliberon.lang.oberon;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.*;

import javax.swing.*;

public class OberonFileType extends LanguageFileType {
  public static final OberonFileType INSTANCE = new OberonFileType();

  private OberonFileType() {
    super(OberonLanguage.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "oberon_src";
  }

  @NotNull
  @Override
  public String getDescription() {
    return "Oberon source";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "aMod";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return OberonIcons.FILE;
  }
}
