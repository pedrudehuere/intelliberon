package com.intelliberon.lang;

import java.util.Stack;

import com.intellij.lexer.FlexLexer;


public abstract class BaseLexer implements FlexLexer
{
    /**
     * Stacking states allows us to keep track of states and return to previous state
     */
    private Stack<Integer> stack = new Stack<Integer>();

    public void yypushState(int newState) {
        stack.push(yystate());
        yybegin(newState);
    }

    public int yypopState() {
        int state = stack.pop();
        yybegin(state);
        return state;
    }
}
