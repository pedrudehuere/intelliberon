# Generates lexer

# This script has been created because it is
# impossible to define the output directory when using
# grammar-kit's "Run JFlex Generator" context menu

# If you are not sure which arguments to pass,
# run "Run JFlex Generator" context menu in the *.flex file
# and copy the values in the generated command

# source-file and destination-dir are relative to the project root
USAGE="$0 java-executable source-file destination-dir"
if [ $# != 3 ]
then
    echo "Usage: ${USAGE}"
    exit 1
fi

java_exec=$1
shift
source_file=$1
shift
destination=$1

project_path=$(realpath "$(dirname "$0")/../")

gen_command="${java_exec} -Xmx512m -Dfile.encoding=UTF-8 -jar
${project_path}/jflex-1.7.0-2.jar -skel
${project_path}/idea-flex.skeleton -d"

destination_dir="${project_path}/${destination}"
source_file="${project_path}/${source_file}"

echo ${gen_command} "${destination_dir}" "${source_file}"
${gen_command} "${destination_dir}" "${source_file}"

if [ $? = 0 ]
then
    echo "SUCCESS"
else
    echo "FAILURE"
fi
